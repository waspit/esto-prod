<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Dyrekcja;
use App\Entity\Jednostka;
use App\Entity\Dzial;
use App\Entity\Pracownik;
use App\Entity\Stanowisko;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Entity\Zadanie;
use App\Comarch\Comarch;
use App\Entity\Opcje;
use App\Entity\Proces;
use App\Entity\ZaplanowanyProces;
use App\Entity\Zlecenie;
use App\Form\ZaplanowanyProcesType;
use App\Form\ZlecenieType;
use DateInterval;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\Types\BooleanType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Response;

class PageController extends AbstractController
{
    /**
     * @Route("/", name="_home")
     */
    public function index()
    {
        return $this->render('page/index.html.twig', [
            'controller_name' => 'PageController',
        ]);
    }

    /**
     * @Route("/zlecenia/aktywne", name="_zlecenia_aktywne")
     */
    public function zleceniaAktywne(EntityManagerInterface $em, PaginatorInterface $paginator, Request $request)
    {

        // $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();

        $sql = '
            SELECT pzl_id FROM zlecenie p
            ';
        $stmt = $conn->prepare($sql);
        $stmt->execute();
    
        
        $nrZP = $stmt->fetchAll();

        $lista = new ArrayCollection();
        foreach ($nrZP as $zlec) {
            $lista->add($zlec['pzl_id']);
        }
        
        // returns an array of Product objects
        

        $com = new Comarch();
        $com->updateZlecenia($em, $lista->toArray());
       // $zlecenia = $em->getRepository(Zlecenie::class)->findAll();
       // $com->aktualizujStatus($em, $zlecenia);
        $zlecenia = $em->getRepository(Zlecenie::class)->findBy(['ZS_Nag' => "zaplanowane w 100%", 'status' => '0'] );
       
        return $this->render('zlecenia/aktywne.html.twig', [
                    'controller_name' => 'PageController',
                    'zlecenia' => $zlecenia
                ]);
    }



     
    
    /**
     * @Route("/zlecenia/usuniete", name="_zlecenia_usuniete")
     */
    public function zleceniaUsuniete(EntityManagerInterface $em, PaginatorInterface $paginator, Request $request)
    {

      
        $repository = $this->getDoctrine()->getRepository(Zlecenie::class); 
        $paginator = $paginator->paginate(
            $repository->findBy(['status' => 2]),$request->query->getInt('page', 1),100); 

        return $this->render('zlecenia/usuniete.html.twig', [
                    'controller_name' => 'PageController',
                    'pagination' => $paginator
                ]);
    }

    /**
     * @Route("/zlecenia/wszystkie", name="_zlecenia_wszystkie")
     */
    public function zleceniaWszystkie()
    {
        $em = $this->getDoctrine()->getManager();
        $zlecenia = $em->getRepository(Zlecenie::class)->findAll();
        return $this->render('zlecenia/wszystkie.html.twig', [
                    'controller_name' => 'PageController',
                    'zlecenia' => $zlecenia
                ]);
    }

    public function menuProcesy()
    {
        $em = $this->getDoctrine()->getManager();
        $procesy = $em->getRepository(Proces::class)->findAll();
        return $this->render('produkcja/procesy.html.twig', [
                    'controller_name' => 'PageController',
                    'procesy' => $procesy
                ]);
    }

    /**
    * @Route("/planuj/{zlecenie}", name="_planuj")
    */
   public function planujZlecenie($zlecenie)
   {
       $em = $this->getDoctrine()->getManager();
       $zlecenie = $em->getRepository(Zlecenie::class)->find($zlecenie);
       $procesy = $em->getRepository(Proces::class)->findAll();
       return $this->render('zlecenia/planuj.html.twig', [
                   'controller_name' => 'PageController',
                   'zlecenie' => $zlecenie,
                   'procesy' => $procesy
               ]);
   }

    /**
    * @Route("/przywroc/{zlecenie}", name="_przywroc")
    */
    public function przywrocZlecenie($zlecenie)
    {
        $em = $this->getDoctrine()->getManager();
        $zlecenie = $em->getRepository(Zlecenie::class)->find($zlecenie);
        $zlecenie->setStatus(0);
        $em->persist($zlecenie);
        $em->flush();
        // $procesy = $em->getRepository(Proces::class)->findAll();
        return $this->redirectToRoute('_zlecenia_usuniete');
    }
/**
    * @Route("/usun/{zlecenie}", name="_usun")
    */
    public function usunZlecenie($zlecenie)
    {
        $em = $this->getDoctrine()->getManager();
        $zlecenie = $em->getRepository(Zlecenie::class)->find($zlecenie);
        $zlecenie->setStatus(2);
        $em->persist($zlecenie);
        $em->flush();
        // $procesy = $em->getRepository(Proces::class)->findAll();
        return $this->redirectToRoute('_zlecenia_aktywne');
    }
    /**
    * @Route("/planuj/zapisz/{zlecenie}", name="_planuj_zapisz")
    */
    public function panujZapisz(Request $request, $zlecenie){
        $em = $this->getDoctrine()->getManager();
        $gdb = $this->getGolemConn();
        $zlecenie = $em->getRepository(Zlecenie::class)->find($zlecenie);
        $post = $request->request->all();
        $size = count($post);
        $size = $size/6;
        for ($i=1; $i <= $size; $i++) { 
            $zaplanowane = new ZaplanowanyProces();
            $idProc =$post['proces-'.$i];
            $proc = $em->getRepository(Proces::class)->find($idProc);
            $zaplanowane->setProces($proc);
            $czas = $post['data-'.$i];
            if($czas != ""){
            $start = \DateTime::createFromFormat('Y/m/d H:i', $czas);
            }
            $ifprze = $post['przezbrojenie-'.$i];
            
            $czascy = $post['czascyk-'.$i];
            $krotnosc = $post['krotnosc-'.$i];
            $czas = 0;
            $czprz = 0;
            if($ifprze == "on"){
                $czprz = $post['czasprzez-'.$i];
                $czprz = $czprz*60;
                $zaplanowane->setCzasPrzezbrojenia($czprz);
            };
            if($czas != ""){
            $czas = $czas+($czascy * floor($zlecenie->getZPEIlosc()/$krotnosc));
            $czas = round($czas,0,PHP_ROUND_HALF_UP);
            $startcp = clone($start);
            $stop = $startcp->add(new DateInterval('PT'.$czas.'S'));
            }
            
            $przez = $post['przezbrojenie-'.$i];
            if($przez == "on"){
                $zaplanowane->setPrzezbrojenie(true);
            }else{
                $zaplanowane->setPrzezbrojenie(false);
            }
            
            
            $zaplanowane->setCzasCyklu($post['czascyk-'.$i]);
            $zaplanowane->setKrotnosc($post['krotnosc-'.$i]);
            if($czas != ""){
            $zaplanowane->setDataStart($start);
            $zaplanowane->setDatastop($stop);
            }
            
            $zaplanowane->setZlecenie($zlecenie);
            $zaplanowane->setWidocznosc(true);
            $zaplanowane->setGotowe(false);
            $czasPro = (($zaplanowane->getCzasCyklu()*$zaplanowane->getZlecenie()->getZPEIlosc())/$zaplanowane->getKrotnosc())+$czprz;
            $zaplanowane->setCzasProd($czasPro);
// SV id maszyny, NAME ->zp, PRODUCT->zpe, TUSED -> 0, Product_id -> nizej, Target -> ilośćm, occ -> czas przejscia, kr -> krotność, 

// Task_product  - NAME -> zse_towar_kod
            // $prodins = "insert into TASK_PRODUCT (NAME, NAMEL , KR, OCC, FC_ID) values 
            // ('$kodproduktu', '$produkt', '$golemkr', '$golemocc', '$golemsv')";
            // $prodimp = ibase_query($dbh, $prodins);	

            $pr_golem = "SELECT ID FROM TASK_PRODUCT WHERE NAME = '".$zlecenie->getZPETowarKod()."' AND STATUS = 0";
            $res_golem = ibase_query($gdb, $pr_golem);
            $prod_id = null;
            if ($row = ibase_fetch_object($res_golem)) {
                $prod_id = $row->ID;
              }
              if($prod_id == null){
                  $name = $zlecenie->getZPETowarKod();
                  $namel = $zlecenie->getZPETowarNazwa();
                  $namel2 = str_replace("'", "`", $namel);
                  $namel2 = str_replace('"', "`", $namel2);
                  $toolid = 0;
                  $status = 0;
                  $m = 1;
                  $allsv = 0;
                  $inpro = "insert into TASK_PRODUCT (M, STATUS, NAME, NAMEL, TOOL_ID, ALLSV) values
                  ('$m', '$status', '$name', '$namel2', '$toolid', '$allsv') returning ID;";
                  $prodgm = ibase_query($gdb, $inpro);
                  if ($row = ibase_fetch_object($prodgm)) {
                    $prod_id = $row->{'ID'};
                  }
              }
              $ilosc = $post['ilosc'];
            //   $sqlins = "insert into TASK_WAITING (ID_OPP, SV , MODE, TUSED, NAME, PRODUCT_ID, TARGET, OP, KR, OCC, PRODUCT, THIDE) values
            //   ('$wprowid', '$golemsv', '0', '0', '$nrzleceniazp', '$golemid', '$zamowione[0]', '$golemop', '$golemkr', '$golemocc', '$kodproduktu','0')";
            $maszyna = $proc->getGolemid();
            $name = $zlecenie->getZPNumer();
           
            $prod = $zlecenie->getZPETowarKod();
            
        
            $cykl = $zaplanowane->getCzasCyklu();
            $cykl = str_replace(',','.',$cykl);
            $krotnosc = $zaplanowane->getKrotnosc();
            $krotnosc = str_replace(',','.',$krotnosc);
            $add_wt = "INSERT INTO TASK_WAITING (SV, MODE, THIDE, NAME, PRODUCT, TORDER, TUSED, PRODUCT_ID, TARGET, OCC, KR) values 
 ('$maszyna', '0', '1','$name', '$prod', '0', '0', '$prod_id', '$ilosc', '$cykl', '$krotnosc') returning ID;";
            $rcaimp = ibase_query($gdb, $add_wt);
            if ($row = ibase_fetch_object($rcaimp)) {
                $prod_waiting = $row->{'ID'};
                $zaplanowane->setGolemWaiting($prod_waiting);
              }
            $zaplanowane->setKolejka(0);
            $em->persist($zaplanowane);
            $em->flush();
            $zlecenie->addZaplanowane($zaplanowane);
        }
        $zlecenie->setStatus(1);
        $zlecenie->setZSEIlosc($ilosc);
        $zlecenie->setZPEIlosc($ilosc);
        
        $em->persist($zlecenie);
        $em->flush();
        return $this->render('zlecenia/zaplanowane.html.twig', [
                   
            ]);
    }

    function random_color_part() {
        return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
    }
    
    function random_color() {
        return $this->random_color_part() . $this->random_color_part() . $this->random_color_part();
    }

    /**
    * @Route("/produkcja", name="_produkcja")
    */
    public function produkcja(){
        $em = $this->getDoctrine()->getManager();
        $procesy = $em->getRepository(Proces::class)->findAll();
        return $this->render('produkcja/gantt.html.twig', [
            'controller_name' => 'PageController',
            'procesy' => $procesy
        ]);
    }



    /**
    * @Route("/move/item", name="_move_item")
    */
    public function moveItem(Request $request){
        $post = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        $moveto = $post["time"]+$post["diff"];
        $sec = floor($moveto/1000);
        $zaplanowany = $em->getRepository(ZaplanowanyProces::class)->find($post["id"]);
        $start = \DateTime::createFromFormat('U', $sec);

        $czas = 0;
        if($zaplanowany->getPrzezbrojenie()){
            $czas = $czas + $zaplanowany->getCzasPrzezbrojenia();
        }
        $czas = $czas + ($zaplanowany->getCzasCyklu() * floor($zaplanowany->getZlecenie()->getZPEIlosc()/$zaplanowany->getKrotnosc()));
        $koniec = clone($start);
             $stop = $koniec->add(new DateInterval('PT'.$czas.'S'));
       $zaplanowany->setDataStart($start);
       $zaplanowany->setDataStop($stop);
       $em->persist($zaplanowany);
       $em->flush();

    }

 /**
    * @Route("/get/item", name="_get_item")
    */
    public function getItem(Request $request){
        $id = $request->request->get("id");
        $em = $this->getDoctrine()->getManager();
        $zap = $em->getRepository(ZaplanowanyProces::class)->find($id);
        $arrData = [
            'id' => $zap->getId(),
            'proces' => $zap->getProces()->getId(),
            'start' => $zap->getDataStart()->format('Y/m/d H:i'),
            'przezbrojenie' => $zap->getPrzezbrojenie(),
            'czasprzez' => $zap->getCzasPrzezbrojenia(),
            'cykl' => $zap->getCzasCyklu(),
            'krotnosc' => $zap->getKrotnosc()
        ];
        return new JsonResponse($arrData);
        

    }
 /**
    * @Route("/save/item", name="_save_item")
    */
    public function saveItem(Request $request){

        $em = $this->getDoctrine()->getManager();
        $post = $request->request->all();
        $size = count($post);
         $zaplanowane = $em->getRepository(ZaplanowanyProces::class)->find($post['planid']);
          
            $idProc =$post['proces'];
            $proc = $em->getRepository(Proces::class)->find($idProc);
            $zaplanowane->setProces($proc);
            $czas = $post['data'];
            $start = \DateTime::createFromFormat('Y/m/d H:i', $czas);
            $ifprze = $post['przezbrojenie'];
            
            $czascy = $post['czascyk'];
            $krotnosc = $post['krotnosc'];
            $czas = 0;
            if($ifprze == "on"){
                $czprz = $post['czasprzez'];
                $czas = $czprz*60;
                $zaplanowane->setCzasPrzezbrojenia($czprz);
            };
            $czas = $czas+($czascy * floor($zaplanowane->getZlecenie()->getZPEIlosc()/$krotnosc));
            $startcp = clone($start);
            $stop = $startcp->add(new DateInterval('PT'.$czas.'S'));
            $zaplanowane->setDataStart($start);
            $przez = $post['przezbrojenie'];
            if($przez == "on"){
                $zaplanowane->setPrzezbrojenie(true);
            }else{
                $zaplanowane->setPrzezbrojenie(false);
            }
            
            $zaplanowane->setCzasCyklu($post['czascyk']);
            $zaplanowane->setKrotnosc($post['krotnosc']);
            $zaplanowane->setDatastop($stop);
            
            $em->persist($zaplanowane);
            $em->flush();
            

        return $this->redirectToRoute('_produkcja');
    }

    /**
    * @Route("/golem", name="_golem")
    */
    public function golem(){
        $db = '185.23.15.42:C:\\golem\\bazy\GOLEM_MES.FDB';
$username = 'SYSDBA';
$password = 'masterkey';
// Connect to database
$dbh = ibase_connect($db, $username, $password);
$kodproduktu='017-01564-01';
$getid = "SELECT ID, OP, KR, OCC FROM TASK_PRODUCT where NAME like '$kodproduktu' and FC_ID = 9";
// Execute query
$rc = ibase_query($dbh, $getid);
return $this->redirectToRoute('_produkcja');
    }
   
    private function getGolemConn(){
        $db = '185.23.15.42:C:\\golem\\bazy\GOLEM_MES.FDB';
        //$db = '192.168.0.211:C:\\golem\\bazy\GOLEM_MES.FDB';
        $username = 'SYSDBA';
        $password = 'masterkey';

        $golemdb = ibase_connect($db, $username, $password);
        return $golemdb;    }


    /**
    * @Route("/produkcja/kolejki-na-procesach", name="_produkcja_lista_zlecen")
    */
    public function produkcjaListaZlecen(Request $request){

        $em = $this->getDoctrine()->getManager();
        $opcje = $em->getRepository(Opcje::class)->find(1);
        $form = $this->createFormBuilder($opcje)
        ->add('listaGotowe')
        ->add('listaWidoczne')
        ->add('zapisz', SubmitType::class, ['label' => 'Filtruj'])
        ->getForm();
        $filtr = array('gotowe'=>$opcje->getListaGotowe(), 'widoczne'=>$opcje->getListaWidoczne());
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
           
            $opt = $form->getData();
            // $test = 0;
            $got = $opt->getListaGotowe();
            $wid = $opt->getListaWidoczne();
            
            $em->persist($opt);
            $em->flush();
            $filtr = array('gotowe'=>$got, 'widoczne'=>$wid);
        }

       
       // $this->updateZaplanowane();

        $procesy = $em->getRepository(Proces::class)->findAll();
        
        foreach ($procesy as $proces) {
            if($proces->getNung() == null && $proces->getUng() == null){

            
            $nung = 0;
            $ung = 0;
            foreach ($proces->getZaplanowanyProces() as $zaplanowany) {
                if($zaplanowany->getCzasProd() == null){
                    $prze = 0;
                if(!is_null($zaplanowany->getCzasPrzezbrojenia())){
                    $prze = $zaplanowany->getCzasPrzezbrojenia();
                }
                    $zaplanowany->setCzasProd((($zaplanowany->getCzasCyklu()*$zaplanowany->getZlecenie()->getZPEIlosc())/$zaplanowany->getKrotnosc())+$prze);
                    $em->persist($zaplanowany);
                    $em->flush();
                }
                
                if($zaplanowany->getWidocznosc() == true and $zaplanowany->getGotowe() == false){
                    $nung += $zaplanowany->getCzasProd();
                }elseif($zaplanowany->getWidocznosc() == false and $zaplanowany->getGotowe() == false){
                    $ung += $zaplanowany->getCzasProd();
                }
                $proces->setNung($nung/3600);
                $proces->setUng($ung/3600);
                set_time_limit(300);
                try {
                        $em->persist($proces);
                        $em->flush();
                    } catch (\Exception $e) {
                /* ... handle the exception */ 
                $em = $this->getDoctrine()->resetManager();
                    }
               
            }
        }
        }
        return $this->render('produkcja/lista_zlecen.html.twig', [
            'controller_name' => 'PageController',
            'procesy' => $procesy,
            'form' => $form->createView(),
            'filtr' => $filtr
        ]);
    }

    /**
    * @Route("/produkcja/lista-zlecen/{id}", name="_produkcja_lista_zlecen_proces")
    */
    public function produkcjaListaZlecenProces(Request $request, $id){
        $em = $this->getDoctrine()->getManager();

        $opcje = $em->getRepository(Opcje::class)->find(1);
        $form = $this->createFormBuilder($opcje)
        ->add('listaGotowe')
        ->add('listaWidoczne')
        ->add('zapisz', SubmitType::class, ['label' => 'Filtruj'])
        ->getForm();
        $filtr = array('gotowe'=>$opcje->getListaGotowe(), 'widoczne'=>$opcje->getListaWidoczne());
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
           
            $opt = $form->getData();
            // $test = 0;
             $got = $opt->getListaGotowe();
             $wid = $opt->getListaWidoczne();
           
            $em->persist($opt);
            $em->flush();
            $filtr = array('gotowe'=>$got, 'widoczne'=>$wid);
        }

        $proces = $em->getRepository(Proces::class)->find($id);
       
           
        
        return $this->render('produkcja/lista_zlecen_proces.html.twig', [
            'controller_name' => 'PageController',
            'proces' => $proces,
            'form' => $form->createView(),
            'filtr' => $filtr
        ]);
    }

     /**
    * @Route("/produkcja/usun-zlecenie/{id}", name="_produkcja_usun_zlecenie")
    */
    public function produkcjaUsunZlecenie($id){
        $em = $this->getDoctrine()->getManager();
        $zap = $em->getRepository(ZaplanowanyProces::class)->find($id);
        $gdb = $this->getGolemConn();
        $golemid = $zap->getGolemWaiting();
        $inpro = "DELETE FROM TASK_WAITING WHERE ID = '$golemid'";
        ibase_query($gdb, $inpro);
        $em->remove($zap);
        $em->flush();         
        return $this->redirectToRoute('_produkcja_lista_zlecen');
    }

    private function updateZaplanowane(){
        $em = $this->getDoctrine()->getManager();
        $gdb = $this->getGolemConn();
        $zaplan = $em->getRepository(ZaplanowanyProces::class)->findAll();
        foreach ($zaplan as $zap) {
            $gc = $this->getGolemConn();
            $pr_golem = "SELECT TUSED, THIDE FROM TASK_WAITING WHERE ID = ".$zap->getGolemWaiting();
            $res_golem = ibase_query($gdb, $pr_golem);
            $prod_id = null;
            if ($row = ibase_fetch_object($res_golem)) {
                $wid = $row->THIDE;
                $uzy = $row->TUSED;
                $zap->setWidocznosc($wid);
                $zap->setGotowe($uzy);
              }
              $em->persist($zap);
              $em->flush();
        }
            
        
    }
    /**
    * @Route("/produkcja/edytuj-ilosc/{id}", name="_produkcja_edytuj_ilosc")
    */
    public function produkcjaZmienIlosc(Request $request, $id){
       
        $em = $this->getDoctrine()->getManager();
        $zaplanowany = $em->getRepository(ZaplanowanyProces::class)->find($id);
        $zlecenie = $zaplanowany->getZlecenie();
        $form = $this->createFormBuilder($zlecenie)
        ->add('ZPE_Ilosc', null, ['label' => 'Ilość', 'attr' => array('class'=>'form-control')])
        ->add('Zapisz', SubmitType::class, ['label' => 'Zapisz', 'attr' => array('class'=>'btn btn-icon icon-left btn-success')])
        
        ->getForm();
        $form->handleRequest($request);
        
        if (!$form->isSubmitted()){
            $ref = $request->server->get('HTTP_REFERER');
            $this->get('session')->set('burl', $ref);
        }
        
        if ($form->isSubmitted()){
            $zlecenie = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $zlecenie->setZSEIlosc($zlecenie->getZPEIlosc());
              $entityManager->persist($zlecenie);
              $entityManager->flush();


              $gdb = $this->getGolemConn();
              foreach ($zlecenie->getZaplanowane() as $zap) {
                $ilosc = $zlecenie->getZPEIlosc();
                $przez = 0;
                
                $zap->setCzasProd((($zap->getCzasCyklu()*$zap->getZlecenie()->getZPEIlosc())/$zap->getKrotnosc())+$zap->getCzasPrzezbrojenia());
                $em->persist($zap);
                $em->flush();
                $golem = $zap->getGolemWaiting();
                  $add_wt = "UPDATE TASK_WAITING SET TARGET =$ilosc WHERE ID=$golem;";
                             $rcaimp = ibase_query($gdb, $add_wt);
              }
              

              return $this->redirect($this->get('session')->get('burl'));
        }
        return $this->render('produkcja/zmien_ilosc.html.twig',[
            'controler_name' => 'PageController',
            'form' => $form->createView(),
            'zlecenie' => $zlecenie
        ]);
    }

  /**
    * @Route("/produkcja/edytuj-zaplanowany-proces/{id}", name="_produkcja_edytuj_plan")
    */
    public function produkcjaEdytujPlan(Request $request, $id){
        $em = $this->getDoctrine()->getManager();
        
        $zaplanowany = $em->getRepository(ZaplanowanyProces::class)->find($id);
        
          $form = $this->createForm(ZaplanowanyProcesType::class, $zaplanowany);
          $form->handleRequest($request);
          if ($form->isSubmitted()){
              $zaplanowany= $form->getData();
            //   $zaplanowany->setProces($zap->getProces());
            //   $zaplanowany->setDataStart($zap->getDataStart());
            //   $zaplanowany->setPrzezbrojenie($zap->getPrzezbrojenie());
            //   $zaplanowany->setCzasPrzezbrojenia($zap->getCzasPrzezbrojenia());
            //   $zaplanowany->setCzasCyklu($zap->getCzasCyklu());
            //   $zaplanowany->setKrotnosc($zap->getKrotnosc());//
            //   $zaplanowany->setWidocznosc($zap)
              
            $prze = 0;
            if(!is_null($zaplanowany->getCzasPrzezbrojenia())){
                $prze = $zaplanowany->getCzasPrzezbrojenia();
            }
                $zaplanowany->setCzasProd((($zaplanowany->getCzasCyklu()*$zaplanowany->getZlecenie()->getZPEIlosc())/$zaplanowany->getKrotnosc())+$prze);

            
              $entityManager = $this->getDoctrine()->getManager();
              $entityManager->persist($zaplanowany);
              $entityManager->flush();
  
              // $add_wt = "INSERT INTO TASK_WAITING (SV, MODE, THIDE, NAME, PRODUCT, TORDER, TUSED, PRODUCT_ID, TARGET, OCC, KR) values 
              // ('$maszyna', '0', '0','$name', '$prod', '0', '0', '$prod_id', '$ilosc', '$cykl', '$krotnosc') returning ID;";
              //            $rcaimp = ibase_query($gdb, $add_wt);
              $gdb = $this->getGolemConn();
              $maszyna = $zaplanowany->getProces()->getGolemid();
              $cykl = $zaplanowany->getCzasCyklu();
              $krotnosc = $zaplanowany->getKrotnosc();
              $gid = $zaplanowany->getGolemWaiting();
              $wid = (int)$zaplanowany->getWidocznosc();
              $got = (int)$zaplanowany->getGotowe();
              $sqlins = "update TASK_WAITING set SV='$maszyna', OCC='$cykl', KR='$krotnosc', TUSED='$got', THIDE='$wid' where ID='$gid'"; 
  
              $rcaimp = ibase_query($gdb, $sqlins);
  
              ibase_close($gdb);
  
              return $this->redirectToRoute('_produkcja_lista_zlecen_proces', array(
                'id' => $zaplanowany->getProces()->getId()
        ));
  
              
              
          }       
              
  
        return $this->render('produkcja/edytuj.html.twig',[
            'controler_name' => 'PageController',
            'form' => $form->createView(),
            'zaplanowany' => $zaplanowany
        ]);
      }

    /**
     * @Route("/zlecenia/dodaj-nowe", name="_zlecenia_dodaj")
     */
    public function zleceniaDodaj(Request $request)
    {
        $zlecenie = new Zlecenie();
        $form = $this->createForm(ZlecenieType::class, $zlecenie);
        $form->handleRequest($request);
        if ($form->isSubmitted()){
            $em = $this->getDoctrine()->getManager();
            $zlecenie->setStatus(0);
            $zlecenie->setKolor($this->randColor());
            $zlecenie->setZPEJm('szt.');
            $zlecenie->setZSNag('zaplanowane w 100%');
            $zlecenie->setZPNumer($zlecenie->getZPNumer());
            $zlecenie->setZPETowarKod($zlecenie->getZSETowarKod());
            $zlecenie->setZPETowarNazwa($zlecenie->getZSETowarNazwa());
            $zlecenie->setZSEIlosc($zlecenie->getZPEIlosc());
            $em->persist($zlecenie);
            $em->flush();
            return $this->redirectToRoute('_zlecenia_aktywne');
        }
        
        return $this->render('zlecenia/dodaj.html.twig', [
            'controller_name' => 'PageController',
            'form' => $form->createView()
            
        ]);
    }
    public function randColor(){
        $hex_string = '#'.bin2hex(openssl_random_pseudo_bytes(3));
        return $hex_string;
    }

    /**
     * @Route("/produkcja/ajax/gotowe/", name="_ajax_gotowe")
     */
    public function ajaxGotowe(Request $request){

        $id = (int)$request->request->get('id');
        $val = $request->request->get('val');
        $bval = $val === 'true'? true: false;
        $em = $this->getDoctrine()->getManager();

        $zap = $em->getRepository(ZaplanowanyProces::class)->find($id);
        $gid = $zap->getGolemWaiting();
        if($bval){
            $qval = 1;
        }else{
            $qval = 0;
        }
        $zap->setGotowe($bval);
        $em->persist($zap);
        $em->flush();
        // try {
        //     $gdb = $this->getGolemConn();
        //     $add_wt = "UPDATE TASK_WAITING SET TUSED = $qval WHERE ID=$gid;";
        //     $rcaimp = ibase_query($gdb, $add_wt);
        // } catch (\Throwable $th) {
        //     //throw $th;
        // }
        
        
            $arrData = ['output' => 'ok'];
            return new JsonResponse($arrData);
        
    }

    /**
     * @Route("/produkcja/ajax/procesy", name="_ajax_procesy")
     */
    public function listaAjaxProcesy(Request $request){

        $em = $this->getDoctrine()->getManager();
        $lista = array();
        $zaplanowane = $em->getRepository(ZaplanowanyProces::class)->findBy(['gotowe' => 0, 'widocznosc' => 0], ['kolejka' => 'ASC']);
        
        foreach ($zaplanowane as $zap) {
            $got = "NIE";
            if($zap->getGotowe()){
                $got = "TAK";
            }
            $wid = "NIE";
            if($zap->getWidocznosc()){
                $wid = "TAK";
            }
            $czasPrze = 0;
            if($zap->getPrzezbrojenie()){
                    $czasPrze = $zap->getCzasPrzezbrojenia();
            }
            $lista[] = ['proces' => $zap->getProces()->getNazwa(),
                        'Kolejka'=> $zap->getKolejka(), 
                        'ZSETowarNazwa' =>  $zap->getZlecenie()->getZSETowarNazwa(),
                        'ZPNumer' => $zap->getZlecenie()->getZPNumer(),
                        'ZSNumer' => $zap->getZlecenie()->getZSNumer(),
                        'ZSETowarKod' => $zap->getZlecenie()->getZSETowarKod(),
                        'ZSETowarNazwa' => $zap->getZlecenie()->getZSETowarNazwa(),
                        'KntAkronim' => $zap->getZlecenie()->getKntAkronim(),
                        'ZSEIlosc' => $zap->getZlecenie()->getZSEIlosc(),
                        'status' => $zap->getZlecenie()->getZSNag(),
                        'czasProd' => number_format((float)$zap->getCzasProd() / 3600, 2, '.', ''),
                        'krotnosc' => $zap->getKrotnosc(),
                        'czasPrze' => $czasPrze
                    
                
        ];

        }
        $response = new JsonResponse();
        $response->setData($lista);

        $response->headers->set('Content-Type', 'application/json');
        return $response;
        
    }
    
    /**
     * @Route("/produkcja/ajax/lista/{id}", name="_ajax_lista_proces")
     */
    public function listaProces(Request $request, $id){

        $em = $this->getDoctrine()->getManager();
        $lista = array();
        $zaplanowane = $em->getRepository(ZaplanowanyProces::class)->findBy(['proces' => $id], ['kolejka' => 'ASC']);
        
        foreach ($zaplanowane as $zap) {
            if($zap->getZlecenie()->getStatus() == 1){
            $got = "NIE";
            if($zap->getGotowe()){
                $got = "TAK";
            }
            $wid = "NIE";
            if($zap->getWidocznosc()){
                $wid = "TAK";
            }
            $lista[] = ['id' => $zap->getId(),
                        'Kolejka'=> $zap->getKolejka(), 
                        'ZSETowarNazwa' =>  $zap->getZlecenie()->getZSETowarNazwa(),
                        'ZPNumer' => $zap->getZlecenie()->getZPNumer(),
                        'ZSNumer' => $zap->getZlecenie()->getZSNumer(),
                        'ZSETowarKod' => $zap->getZlecenie()->getZSETowarKod(),
                        'ZSETowarNazwa' => $zap->getZlecenie()->getZSETowarNazwa(),
                        'KntAkronim' => $zap->getZlecenie()->getKntAkronim(),
                        'ZSEIlosc' => $zap->getZlecenie()->getZSEIlosc(),
                        'status' => $zap->getZlecenie()->getZSNag(),
                        'czasProd' => number_format((float)$zap->getCzasProd() / 3600, 2, '.', ''),
                        'gotowe' => $got,
                        'widocznosc' => $wid
                    
                
        ];
    }
        }
        $response = new JsonResponse();
        $response->setData($lista);

        $response->headers->set('Content-Type', 'application/json');
        return $response;
        
    }

/**
     * @Route("/produkcja/ajax/ukryte/", name="_ajax_ukryte")
     */
    public function ajaxUkryte(Request $request){

        $id = (int)$request->request->get('id');
        $val = $request->request->get('val');
        $bval = $val === 'true'? true : false;
        $em = $this->getDoctrine()->getManager();

        $zap = $em->getRepository(ZaplanowanyProces::class)->find($id);
        $gid = $zap->getGolemWaiting();
        if($bval){
            $qval = 1;
        }else{
            $qval = 0;
        }
        $zap->setWidocznosc($bval);
        $em->persist($zap);
        $em->flush();
        // try {
        //     $gdb = $this->getGolemConn();
        //     $add_wt = "UPDATE TASK_WAITING SET THIDE = $qval WHERE ID=$gid;";
        //     $rcaimp = ibase_query($gdb, $add_wt);
        // } catch (\Throwable $th) {
        //     //throw $th;
        // }
        // $gdb = $this->getGolemConn();
        //     $add_wt = "UPDATE TASK_WAITING SET THIDE = $qval WHERE ID=$gid;";
        //     $rcaimp = ibase_query($gdb, $add_wt);
        
        
            $arrData = ['output' => 'ok'];
            return new JsonResponse($arrData);
        
    }

    /**
     * @Route("/produkcja/ajax/pozycja/", name="_ajax_pozycja")
     */
    public function ajaxPozycja(Request $request){

        $id = (int)$request->request->get('id');
        $val = $request->request->get('val');
       
        $em = $this->getDoctrine()->getManager();

        $zap = $em->getRepository(ZaplanowanyProces::class)->find($id);
        $gid = $zap->getGolemWaiting();
        $zap->setKolejka($val);
        $em->persist($zap);
        $em->flush();
        $gdb = $this->getGolemConn();
            $add_wt = "UPDATE TASK_WAITING SET TORDER = $val WHERE ID=$gid;";
            $rcaimp = ibase_query($gdb, $add_wt);
        
            $arrData = ['output' => 'ok'];
            return new JsonResponse($arrData);
        
    }

    /**
     * @Route("/produkcja/ajax/czas/", name="_ajax_czas")
     */
    public function ajaxCzas(Request $request){

        $em = $this->getDoctrine()->getManager();
        $id = (int)$request->request->get('id');
        $proces = $em->getRepository(Proces::class)->find($id);
       
            $nung = 0;
            $ung = 0;
            foreach ($proces->getZaplanowanyProces() as $zaplanowany) {

                if($zaplanowany->getCzasProd() == null){
                    $prze = 0;
                if(!is_null($zaplanowany->getCzasPrzezbrojenia())){
                    $prze = $zaplanowany->getCzasPrzezbrojenia();
                }
                    $zaplanowany->setCzasProd((($zaplanowany->getCzasCyklu()*$zaplanowany->getZlecenie()->getZPEIlosc())/$zaplanowany->getKrotnosc())+$prze);
                    $em->persist($zaplanowany);
                    $em->flush();
                }
                
                if($zaplanowany->getWidocznosc() == true and $zaplanowany->getGotowe() == false){
                    $nung += $zaplanowany->getCzasProd();
                }elseif($zaplanowany->getWidocznosc() == false and $zaplanowany->getGotowe() == false){
                    $ung += $zaplanowany->getCzasProd();
                }
                $proces->setNung($nung/3600);
                $proces->setUng($ung/3600);
                set_time_limit(300);
                try {
                        $em->persist($proces);
                        $em->flush();
                    } catch (\Exception $e) {
                /* ... handle the exception */ 
                $em = $this->getDoctrine()->resetManager();
                    }
               
            }

            $arrData = ['nung' => $nung/3600, 'ung' => $ung/3600];
            return new JsonResponse($arrData);
    }
}
