<?php

namespace App\Form;

use App\Entity\Zlecenie;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
class ZlecenieType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('ZS_Numer', null, ['label' => 'Numer ZS', 'attr' => array('class'=>'form-control')])
            ->add('ZP_Numer', null, ['label' => 'Numer ZP', 'attr' => array('class'=>'form-control')])
            ->add('ZPE_Ilosc', null, ['label' => 'Ilość', 'attr' => array('class'=>'form-control')])
           
            ->add('ZSE_TowarKod', null, ['label' => 'Kod towaru', 'attr' => array('class'=>'form-control')])
            ->add('ZSE_TowarNazwa', null, ['label' => 'Nazwa', 'attr' => array('class'=>'form-control')])
            
            ->add('Knt_Akronim', null, ['label' => 'Klient', 'attr' => array('class'=>'form-control')])
            // ->add('PZL_Id', null, ['label' => 'PZL ID', 'attr' => array('class'=>'form-control')])
            
            ->add('dodaj', SubmitType::class, ['label' => 'Dodaj', 'attr' => array('class'=>'btn btn-icon icon-left btn-success')])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Zlecenie::class,
        ]);
    }
}
