<?php

namespace App\Form;

use App\Entity\Proces;
use App\Entity\ZaplanowanyProces;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ZaplanowanyProcesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dataStart')
           
            ->add('przezbrojenie')
            ->add('czasPrzezbrojenia')
            ->add('czasCyklu')
            ->add('krotnosc')    
            ->add('widocznosc')  
            ->add('gotowe')  
            ->add('proces', EntityType::class,[
                'class' => Proces::class,
                'choice_label' => 'nazwa' 
            ])
            ->add('zapisz', SubmitType::class, ['label' => 'Zapisz zmiany'])
            
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ZaplanowanyProces::class,
        ]);
    }
}
