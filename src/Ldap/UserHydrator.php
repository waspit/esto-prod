<?php

namespace App\Ldap;

use FR3D\LdapBundle\Hydrator\HydratorInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\ORM\EntityManager;
use App\Entity\Jednostka;
use App\Entity\User;

class UserHydrator implements HydratorInterface {

    /**
     * Populate an user with the data retrieved from LDAP.
     *
     * @param array $ldapEntry LDAP result information as a multi-dimensional array.
     *              see {@link http://www.php.net/function.ldap-get-entries.php} for array format examples.
     *
     * @return UserInterface
     */
    protected $em;

    public function __construct(EntityManager $entityManager) {

        $this->em = $entityManager;
    }

    public function hydrate(array $ldapEntry): UserInterface {


        if (!$ldapEntry) {
            return null;
        }

        if (!array_key_exists('samaccountname', $ldapEntry)) {
            return null;
        }

            $dn = $ldapEntry['distinguishedname'][0];
            $explodeDN = explode(",OU=", $dn);
            $jednostkaName = $explodeDN[1];
            $jednostka = $this->em->getRepository(Jednostka::class)->findOneBy(array("nazwa" => $jednostkaName));
        
        
        $username = $ldapEntry['samaccountname'][0];
        $userFromDB = $this->em->getRepository(User::class)->findOneBy(['username' => $username]);
        if ($userFromDB == null) {
            $user = new User();
            $user->setUsername($ldapEntry['samaccountname'][0]);

            $user->setFirstname($ldapEntry['givenname'][0]);
            $user->setLastname($ldapEntry['sn'][0]);
            $user->setPassword('@wmLh_Np.5z!"5M-');
            $user->setEnabled(true);

            $user->setRoles(array('ROLE_USER'));
            $user->setEmail($ldapEntry['mail'][0]);

            $user->setJednostka($jednostka);
        } else {
            if($userFromDB->getJednostka() != $jednostka){
                $userFromDB->setJednostka($jednostka);
                $this->em->persist($userFromDB);
                $this->em->flush();
            }
            $user = $userFromDB;
        }













        return $user;
    }

}
