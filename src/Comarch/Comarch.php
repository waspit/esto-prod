<?php
namespace App\Comarch;

use App\Entity\Zlecenie;

class Comarch{

    private $serverName = "tcp:192.168.0.210\\COMARCH, 1433"; //serverName\instanceName
    private $connectionInfo = array( "Database"=>"ERPXL_ESTO2019", "UID"=>"sa", "PWD"=>"DataDBxl$", "CharacterSet" => "UTF-8");
    private $conn;

// if( $conn ) {
//      echo "<h4>Połączenie do XL poprawne.</h4><br />";

// //echo "<br>";  echo $row[0]; echo "<br>";


// }else{
//      echo "Bak połączenia do bazy danych <br />"; 
//      die( print_r( sqlsrv_errors(), true));
// }

    private function connect(){
         //$serverName = "tcp:185.23.15.42\\COMARCH, 4133"; //serverName\instanceName
         $serverName = "tcp:192.168.0.210\\COMARCH, 4133"; //serverName\instanceName
         $connectionInfo = array( "Database"=>"ERPXL_ESTO2019", "UID"=>"sa", "PWD"=>"DataDBxl$", "CharacterSet" => "UTF-8");
            $conn = sqlsrv_connect( $serverName, $connectionInfo);
        
        return $conn;
       }

       public function updateZlecenia($em, $nrZP)
       {
        try {
           $conn = $this->connect();
           $sprz = implode( "','", $nrZP);
            $sprz = "'".$sprz."'";
           $tsql = "SELECT [ZS_Numer]=CDN.NumerDokumentu(CDN.DokMapTypDokumentu(ZaN_GIDTyp,ZaN_ZamTyp,ZaN_Rodzaj),0,0,ZaN_ZamNumer,ZaN_ZamRok,ZaN_ZamSeria,ZaN_ZamMiesiac) , [ZP_Numer] = CONCAT('ZP-',ProdZlecenia.PZL_Numer, '/', ProdZlecenia.PZL_Miesiac,'/', ProdZlecenia.PZL_Rok), [ZPE_TowarKod] = Twr_Kod, [ZPE_TowarNazwa] = Twr_Nazwa, [ZPE_Ilosc] = PZE_Ilosc, [ZPE_Jm] = Twr_Jm,  [ZSE_TowarKod] = ZaE_TwrKod, [ZSE_TowarNazwa]= ZaE_TwrNazwa, [ZSE_Ilosc] = ZaE_Ilosc, [ZS_Nag] = PZL_Status, [Knt_Akronim] = Knt_Akronim, PZL_Id FROM CDN.ProdZlecenia left outer join CDN.ProdZlecElem on PzL_ID=PZE_Zlecenie left outer join CDN.TwrKarty on PZE_TwrNumer=Twr_GIDNumer and PZE_TwrTyp=Twr_GIDTyp 	left outer join CDN.ZamZamLinki on ZZL_ZZGidNumer = PZE_Id inner join CDN.ZamElem on ZaE_GIDNumer=ZZL_ZSGidNumer and ZaE_GIDTyp=ZZL_ZSGidTyp and ZaE_GIDLp=ZZL_ZSGidLp left outer join CDN.ZamNag on ZaN_GIDNumer=ZaE_GIDNumer and ZaN_GIDTyp=ZaE_GIDTyp left outer join CDN.KntKarty on Knt_GIDNumer=PZL_KntNumer where PZL_Status='zaplanowane w 100%' and PZL_Id not in ($sprz)  order by PZL_Id";
        
           $stmt = sqlsrv_query( $conn, $tsql); 
           $params = array();
            $options =  array( "Scrollable" => "SQLSRV_CURSOR_KEYSET" );
           $stmtc = sqlsrv_query( $conn, $tsql , $params, $options );


           while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {

            $zlec = new Zlecenie();
            $zlec->setZSNumer($row["ZS_Numer"]);
            $zlec->setZPNumer($row["ZP_Numer"]);
            $zlec->setZPETowarKod($row["ZPE_TowarKod"]);
            $zlec->setZPETowarNazwa($row["ZPE_TowarNazwa"]);
            $zlec->setZPEIlosc($row["ZPE_Ilosc"]);
            $zlec->setZPEJm($row["ZPE_Jm"]);
            $zlec->setZSETowarKod($row["ZSE_TowarKod"]);
            $zlec->setZSETowarNazwa($row["ZSE_TowarNazwa"]);
            $zlec->setZSEIlosc($row["ZSE_Ilosc"]);
            $zlec->setZSNag($row["ZS_Nag"]);
            $zlec->setKntAkronim($row["Knt_Akronim"]);
            $zlec->setPZLId($row["PZL_Id"]);
            $zlec->setStatus($row["PZL_Status"]);
            $zlec->setKolor($this->randColor());
            $em->persist($zlec);
            $em->flush();
      }
    } catch (\Exception $e) {
    }
      

#            $numRows = sqlsrv_num_rows( $stmtc );
 #           $numFields = sqlsrv_num_fields( $stmt );
 #$i = 1;
 #           while( sqlsrv_fetch( $stmt )) {
  #              for($i = 0; $i < $numFields; $i++) { 
   #                 while($wartosc != false){
  #                  $wartosc=sqlsrv_get_field($stmt, $i);
   #                 $i++;
   #                 }
                    
                         
   #                 }
       #     }
       }

       public function aktualizujStatus($em, $zlecenia)
      {
        try {
        $conn = $this->connect();
          foreach ($zlecenia as $zlec) {
              $num = $zlec->getPZLId();
                  # SELECT PZL_Status FROM CDN.ProdZlecenia WHERE PZL_Id=13206 -- ZP/88/9/2020
            $tsql = "SELECT PZL_Status FROM CDN.ProdZlecenia WHERE PZL_Id=$num";
           
           $stmt = sqlsrv_query( $conn, $tsql); 
           $params = array();
            $options =  array( "Scrollable" => "SQLSRV_CURSOR_KEYSET" );
           $stmtc = sqlsrv_query( $conn, $tsql , $params, $options );
           set_time_limit(300);
           
           while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {
            try {
              // first transaction
              $zlec->setStatus($row["PZL_Status"]);
             $em->persist($zlec);
             $em->flush();
          } catch (\Exception $e) {
              /* ... handle the exception */ 
              $em->resetManager();
          }
             
            
           }
          
          //   $stmt = sqlsrv_query( $conn, $zap); 
          //  $params = array();
          //   $options =  array( "Scrollable" => "SQLSRV_CURSOR_KEYSET" );
          //  $stmtc = sqlsrv_query( $conn, $zap , $params, $options );
          //  while( $row = sqlsrv_fetch_array( $stmtc, SQLSRV_FETCH_ASSOC) ) {
          //   $test = 1;
          // }
        }
        // $tsql = "SELECT 
        // [ZS_Numer]=CDN.NumerDokumentu(CDN.DokMapTypDokumentu(ZaN_GIDTyp,ZaN_ZamTyp,ZaN_Rodzaj),0,0,ZaN_ZamNumer,ZaN_ZamRok,ZaN_ZamSeria,ZaN_ZamMiesiac) , 
        // [ZP_Numer] = CONCAT('ZP-',ProdZlecenia.PZL_Numer, '/', ProdZlecenia.PZL_Miesiac,'/', ProdZlecenia.PZL_Rok), 
        // [ZPE_TowarKod] = Twr_Kod, 
        // [ZPE_TowarNazwa] = Twr_Nazwa, 
        // [ZPE_Ilosc] = PZE_Ilosc, 
        // [ZPE_Jm] = Twr_Jm, 
        //  [ZSE_TowarKod] = ZaE_TwrKod, 
        //  [ZSE_TowarNazwa]= ZaE_TwrNazwa, [ZSE_Ilosc] = ZaE_Ilosc,
        //   [ZS_Nag] = PZL_Status, [Knt_Akronim] = Knt_Akronim, PZL_Id 
        //   FROM CDN.ProdZlecenia left outer join CDN.ProdZlecElem 
        //   on PzL_ID=PZE_Zlecenie left outer join CDN.TwrKarty 
        //   on PZE_TwrNumer=Twr_GIDNumer and PZE_TwrTyp=Twr_GIDTyp 	
        //   left outer join CDN.ZamZamLinki on ZZL_ZZGidNumer = PZE_Id inner join CDN.ZamElem 
        //   on ZaE_GIDNumer=ZZL_ZSGidNumer and ZaE_GIDTyp=ZZL_ZSGidTyp and ZaE_GIDLp=ZZL_ZSGidLp left outer join CDN.ZamNag 
        //   on ZaN_GIDNumer=ZaE_GIDNumer and ZaN_GIDTyp=ZaE_GIDTyp left outer join CDN.KntKarty on Knt_GIDNumer=PZL_KntNumer 
        //   where PZL_Status='zaplanowane w 100%' and PZL_Id not in ($sprz)  order by PZL_Id";
      } catch (\Exception $e) {
      }
      }

       public function randColor(){
        $hex_string = '#'.bin2hex(openssl_random_pseudo_bytes(3));
        return $hex_string;
    }

}
