<?php

namespace App\Repository;

use App\Entity\Dyrekcja;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Dyrekcja|null find($id, $lockMode = null, $lockVersion = null)
 * @method Dyrekcja|null findOneBy(array $criteria, array $orderBy = null)
 * @method Dyrekcja[]    findAll()
 * @method Dyrekcja[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DyrekcjaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Dyrekcja::class);
    }

    // /**
    //  * @return Dyrekcja[] Returns an array of Dyrekcja objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Dyrekcja
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
