<?php

namespace App\Repository;

use App\Entity\Dzial;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Dzial|null find($id, $lockMode = null, $lockVersion = null)
 * @method Dzial|null findOneBy(array $criteria, array $orderBy = null)
 * @method Dzial[]    findAll()
 * @method Dzial[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DzialRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Dzial::class);
    }

    // /**
    //  * @return Dzial[] Returns an array of Dzial objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Dzial
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
