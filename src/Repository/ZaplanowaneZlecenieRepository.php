<?php

namespace App\Repository;

use App\Entity\ZaplanowaneZlecenie;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ZaplanowaneZlecenie|null find($id, $lockMode = null, $lockVersion = null)
 * @method ZaplanowaneZlecenie|null findOneBy(array $criteria, array $orderBy = null)
 * @method ZaplanowaneZlecenie[]    findAll()
 * @method ZaplanowaneZlecenie[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ZaplanowaneZlecenieRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ZaplanowaneZlecenie::class);
    }

    // /**
    //  * @return ZaplanowaneZlecenie[] Returns an array of ZaplanowaneZlecenie objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('z')
            ->andWhere('z.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('z.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ZaplanowaneZlecenie
    {
        return $this->createQueryBuilder('z')
            ->andWhere('z.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
