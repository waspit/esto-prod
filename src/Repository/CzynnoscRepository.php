<?php

namespace App\Repository;

use App\Entity\Czynnosc;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Czynnosc|null find($id, $lockMode = null, $lockVersion = null)
 * @method Czynnosc|null findOneBy(array $criteria, array $orderBy = null)
 * @method Czynnosc[]    findAll()
 * @method Czynnosc[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CzynnoscRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Czynnosc::class);
    }

    // /**
    //  * @return Czynnosc[] Returns an array of Czynnosc objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Czynnosc
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
