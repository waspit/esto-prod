<?php

namespace App\Repository;

use App\Entity\Opcje;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Opcje|null find($id, $lockMode = null, $lockVersion = null)
 * @method Opcje|null findOneBy(array $criteria, array $orderBy = null)
 * @method Opcje[]    findAll()
 * @method Opcje[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OpcjeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Opcje::class);
    }

    // /**
    //  * @return Opcje[] Returns an array of Opcje objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Opcje
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
