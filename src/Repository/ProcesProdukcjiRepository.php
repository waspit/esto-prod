<?php

namespace App\Repository;

use App\Entity\ProcesProdukcji;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ProcesProdukcji|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProcesProdukcji|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProcesProdukcji[]    findAll()
 * @method ProcesProdukcji[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProcesProdukcjiRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProcesProdukcji::class);
    }

    // /**
    //  * @return ProcesProdukcji[] Returns an array of ProcesProdukcji objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProcesProdukcji
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
