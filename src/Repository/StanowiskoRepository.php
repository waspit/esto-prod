<?php

namespace App\Repository;

use App\Entity\Stanowisko;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Stanowisko|null find($id, $lockMode = null, $lockVersion = null)
 * @method Stanowisko|null findOneBy(array $criteria, array $orderBy = null)
 * @method Stanowisko[]    findAll()
 * @method Stanowisko[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StanowiskoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Stanowisko::class);
    }

    // /**
    //  * @return Stanowisko[] Returns an array of Stanowisko objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Stanowisko
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
