<?php

namespace App\Repository;

use App\Entity\ZaplanowanyProces;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ZaplanowanyProces|null find($id, $lockMode = null, $lockVersion = null)
 * @method ZaplanowanyProces|null findOneBy(array $criteria, array $orderBy = null)
 * @method ZaplanowanyProces[]    findAll()
 * @method ZaplanowanyProces[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ZaplanowanyProcesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ZaplanowanyProces::class);
    }

    // /**
    //  * @return ZaplanowanyProces[] Returns an array of ZaplanowanyProces objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('z')
            ->andWhere('z.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('z.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ZaplanowanyProces
    {
        return $this->createQueryBuilder('z')
            ->andWhere('z.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
