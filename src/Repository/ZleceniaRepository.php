<?php

namespace App\Repository;

use App\Entity\Zlecenia;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Zlecenia|null find($id, $lockMode = null, $lockVersion = null)
 * @method Zlecenia|null findOneBy(array $criteria, array $orderBy = null)
 * @method Zlecenia[]    findAll()
 * @method Zlecenia[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ZleceniaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Zlecenia::class);
    }

    // /**
    //  * @return Zlecenia[] Returns an array of Zlecenia objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('z')
            ->andWhere('z.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('z.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Zlecenia
    {
        return $this->createQueryBuilder('z')
            ->andWhere('z.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
