<?php

namespace App\Repository;

use App\Entity\Zlecenie;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Zlecenie|null find($id, $lockMode = null, $lockVersion = null)
 * @method Zlecenie|null findOneBy(array $criteria, array $orderBy = null)
 * @method Zlecenie[]    findAll()
 * @method Zlecenie[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ZlecenieRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Zlecenie::class);
    }

    // /**
    //  * @return Zlecenie[] Returns an array of Zlecenie objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('z')
            ->andWhere('z.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('z.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Zlecenie
    {
        return $this->createQueryBuilder('z')
            ->andWhere('z.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
