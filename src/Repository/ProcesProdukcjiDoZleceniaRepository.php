<?php

namespace App\Repository;

use App\Entity\ProcesProdukcjiDoZlecenia;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ProcesProdukcjiDoZlecenia|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProcesProdukcjiDoZlecenia|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProcesProdukcjiDoZlecenia[]    findAll()
 * @method ProcesProdukcjiDoZlecenia[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProcesProdukcjiDoZleceniaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProcesProdukcjiDoZlecenia::class);
    }

    // /**
    //  * @return ProcesProdukcjiDoZlecenia[] Returns an array of ProcesProdukcjiDoZlecenia objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProcesProdukcjiDoZlecenia
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
