<?php

namespace App\Repository;

use App\Entity\Operatorzy;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Operatorzy|null find($id, $lockMode = null, $lockVersion = null)
 * @method Operatorzy|null findOneBy(array $criteria, array $orderBy = null)
 * @method Operatorzy[]    findAll()
 * @method Operatorzy[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OperatorzyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Operatorzy::class);
    }

    // /**
    //  * @return Operatorzy[] Returns an array of Operatorzy objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Operatorzy
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
