<?php

namespace App\Repository;

use App\Entity\Zadanie;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Zadanie|null find($id, $lockMode = null, $lockVersion = null)
 * @method Zadanie|null findOneBy(array $criteria, array $orderBy = null)
 * @method Zadanie[]    findAll()
 * @method Zadanie[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ZadanieRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Zadanie::class);
    }

    // /**
    //  * @return Zadanie[] Returns an array of Zadanie objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('z')
            ->andWhere('z.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('z.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Zadanie
    {
        return $this->createQueryBuilder('z')
            ->andWhere('z.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
