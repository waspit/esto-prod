<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ZaplanowanyProcesRepository")
 */
class ZaplanowanyProces
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Proces", inversedBy="zaplanowanyProces")
     * @ORM\JoinColumn(nullable=false)
     */
    private $proces;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dataStart;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $datastop;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $przezbrojenie;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $czasPrzezbrojenia;

    /**
     * @ORM\Column(type="float")
     */
    private $czasCyklu;

    /**
     * @ORM\Column(type="float")
     */
    private $krotnosc;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Zlecenie", inversedBy="zaplanowane")
     */
    private $zlecenie;

    /**
     * @ORM\Column(type="integer")
     */
    private $golem_waiting;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $status;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $widocznosc;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $gotowe;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $kolejka;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $czasProd;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProces(): ?Proces
    {
        return $this->proces;
    }

    public function setProces(?Proces $proces): self
    {
        $this->proces = $proces;

        return $this;
    }

    public function getDataStart(): ?\DateTimeInterface
    {
        return $this->dataStart;
    }

    public function setDataStart(\DateTimeInterface $dataStart): self
    {
        $this->dataStart = $dataStart;

        return $this;
    }

    public function getDatastop(): ?\DateTimeInterface
    {
        return $this->datastop;
    }

    public function setDatastop(\DateTimeInterface $datastop): self
    {
        $this->datastop = $datastop;

        return $this;
    }

    public function getPrzezbrojenie(): ?bool
    {
        return $this->przezbrojenie;
    }

    public function setPrzezbrojenie(?bool $przezbrojenie): self
    {
        $this->przezbrojenie = $przezbrojenie;

        return $this;
    }

    public function getCzasPrzezbrojenia(): ?float
    {
        $czas = 0;
        if($this->czasPrzezbrojenia){
            $czas = $this->czasPrzezbrojenia;
        }
        return $czas;
    }

    public function setCzasPrzezbrojenia(?float $czasPrzezbrojenia): self
    {
        $this->czasPrzezbrojenia = $czasPrzezbrojenia;

        return $this;
    }

    public function getCzasCyklu(): ?float
    {
        return $this->czasCyklu;
    }

    public function setCzasCyklu(float $czasCyklu): self
    {
        $this->czasCyklu = $czasCyklu;

        return $this;
    }

    public function getKrotnosc(): ?float
    {
        return $this->krotnosc;
    }

    public function setKrotnosc(float $krotnosc): self
    {
        $this->krotnosc = $krotnosc;

        return $this;
    }

    public function getZlecenie(): ?Zlecenie
    {
        return $this->zlecenie;
    }

    public function setZlecenie(?Zlecenie $zlecenie): self
    {
        $this->zlecenie = $zlecenie;

        return $this;
    }

    public function getGolemWaiting(): ?int
    {
        return $this->golem_waiting;
    }

    public function setGolemWaiting(int $golem_waiting): self
    {
        $this->golem_waiting = $golem_waiting;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(?int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getWidocznosc(): ?bool
    {
        return $this->widocznosc;
    }

    public function setWidocznosc(?bool $widocznosc): self
    {
        $this->widocznosc = $widocznosc;

        return $this;
    }

    public function getGotowe(): ?bool
    {
        return $this->gotowe;
    }

    public function setGotowe(?bool $gotowe): self
    {
        $this->gotowe = $gotowe;

        return $this;
    }

    public function getKolejka(): ?int
    {
        return $this->kolejka;
    }

    public function setKolejka(?int $kolejka): self
    {
        $this->kolejka = $kolejka;

        return $this;
    }

    public function getCzasProd(): ?float
    {
        return $this->czasProd;
    }

    public function setCzasProd(?float $czasProd): self
    {
        $this->czasProd = $czasProd;

        return $this;
    }
}
