<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProcesRepository")
 */
class Proces
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $nazwa;

    /**
     * @ORM\Column(type="float")
     */
    private $czas1szt;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ZaplanowanyProces", mappedBy="proces")
     */
    private $zaplanowanyProces;

    /**
     * @ORM\Column(type="integer")
     */
    private $golemid;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $przezbrojenie;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $nung;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $ung;

    public function __construct()
    {
        $this->zaplanowanyProces = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNazwa(): ?string
    {
        return $this->nazwa;
    }

    public function setNazwa(string $nazwa): self
    {
        $this->nazwa = $nazwa;

        return $this;
    }

    public function getCzas1szt(): ?float
    {
        return $this->czas1szt;
    }

    public function setCzas1szt(float $czas1szt): self
    {
        $this->czas1szt = $czas1szt;

        return $this;
    }

    /**
     * @return Collection|ZaplanowanyProces[]
     */
    public function getZaplanowanyProces(): Collection
    {
        return $this->zaplanowanyProces;
    }

    /**
     * @return Collection|ZaplanowanyProces[]
     */
    public function getProcesyKolejka(bool $got, bool $wid): Collection
    {
        $zps = $this->getZaplanowanyProces();
        foreach ($zps as $zp) {
            if($zp->getGotowe() != $got || $zp->getWidocznosc() != $wid){
                $zps->removeElement($zp);
            }
        }
        return $zps;
    }

    public function addZaplanowanyProce(ZaplanowanyProces $zaplanowanyProce): self
    {
        if (!$this->zaplanowanyProces->contains($zaplanowanyProce)) {
            $this->zaplanowanyProces[] = $zaplanowanyProce;
            $zaplanowanyProce->setProces($this);
        }

        return $this;
    }

    public function removeZaplanowanyProce(ZaplanowanyProces $zaplanowanyProce): self
    {
        if ($this->zaplanowanyProces->contains($zaplanowanyProce)) {
            $this->zaplanowanyProces->removeElement($zaplanowanyProce);
            // set the owning side to null (unless already changed)
            if ($zaplanowanyProce->getProces() === $this) {
                $zaplanowanyProce->setProces(null);
            }
        }

        return $this;
    }

    public function getGolemid(): ?int
    {
        return $this->golemid;
    }

    public function setGolemid(int $golemid): self
    {
        $this->golemid = $golemid;

        return $this;
    }

    public function getPrzezbrojenie(): ?float
    {
        return $this->przezbrojenie;
    }

    public function setPrzezbrojenie(?float $przezbrojenie): self
    {
        $this->przezbrojenie = $przezbrojenie;

        return $this;
    }

    public function getNung(): ?float
    {
        return $this->nung;
    }

    public function setNung(?float $nung): self
    {
        $this->nung = $nung;

        return $this;
    }

    public function getUng(): ?float
    {
        return $this->ung;
    }

    public function setUng(?float $ung): self
    {
        $this->ung = $ung;

        return $this;
    }
}
