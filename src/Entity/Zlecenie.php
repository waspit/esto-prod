<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ZlecenieRepository")
 */
class Zlecenie
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=32)
     */
    private $ZS_Numer;

    /**
     * @ORM\Column(type="string", length=32)
     */
    private $ZP_Numer;

    /**
     * @ORM\Column(type="string", length=32)
     */
    private $ZPE_TowarKod;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ZPE_TowarNazwa;

    /**
     * @ORM\Column(type="integer")
     */
    private $ZPE_Ilosc;

    /**
     * @ORM\Column(type="string", length=32)
     */
    private $ZPE_Jm;

    /**
     * @ORM\Column(type="string", length=32)
     */
    private $ZSE_TowarKod;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ZSE_TowarNazwa;

    /**
     * @ORM\Column(type="integer")
     */
    private $ZSE_Ilosc;

    /**
     * @ORM\Column(type="string", length=32)
     */
    private $ZS_Nag;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Knt_Akronim;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $PZL_Id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $status;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ZaplanowanyProces", mappedBy="zlecenie")
     */
    private $zaplanowane;

    /**
     * @ORM\Column(type="string", length=7)
     */
    private $kolor;

    public function __construct()
    {
        $this->zaplanowane = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getZSNumer(): ?string
    {
        return $this->ZS_Numer;
    }

    public function setZSNumer(string $ZS_Numer): self
    {
        $this->ZS_Numer = $ZS_Numer;

        return $this;
    }

    public function getZPNumer(): ?string
    {
        return $this->ZP_Numer;
    }

    public function setZPNumer(string $ZP_Numer): self
    {
        $this->ZP_Numer = $ZP_Numer;

        return $this;
    }

    public function getZPETowarKod(): ?string
    {
        return $this->ZPE_TowarKod;
    }

    public function setZPETowarKod(string $ZPE_TowarKod): self
    {
        $this->ZPE_TowarKod = $ZPE_TowarKod;

        return $this;
    }

    public function getZPETowarNazwa(): ?string
    {
        return $this->ZPE_TowarNazwa;
    }

    public function setZPETowarNazwa(string $ZPE_TowarNazwa): self
    {
        $this->ZPE_TowarNazwa = $ZPE_TowarNazwa;

        return $this;
    }

    public function getZPEIlosc(): ?int
    {
        return $this->ZPE_Ilosc;
    }

    public function setZPEIlosc(int $ZPE_Ilosc): self
    {
        $this->ZPE_Ilosc = $ZPE_Ilosc;

        return $this;
    }

    public function getZPEJm(): ?string
    {
        return $this->ZPE_Jm;
    }

    public function setZPEJm(string $ZPE_Jm): self
    {
        $this->ZPE_Jm = $ZPE_Jm;

        return $this;
    }

    public function getZSETowarKod(): ?string
    {
        return $this->ZSE_TowarKod;
    }

    public function setZSETowarKod(string $ZSE_TowarKod): self
    {
        $this->ZSE_TowarKod = $ZSE_TowarKod;

        return $this;
    }

    public function getZSETowarNazwa(): ?string
    {
        return $this->ZSE_TowarNazwa;
    }

    public function setZSETowarNazwa(string $ZSE_TowarNazwa): self
    {
        $this->ZSE_TowarNazwa = $ZSE_TowarNazwa;

        return $this;
    }

    public function getZSEIlosc(): ?int
    {
        return $this->ZSE_Ilosc;
    }

    public function setZSEIlosc(int $ZSE_Ilosc): self
    {
        $this->ZSE_Ilosc = $ZSE_Ilosc;

        return $this;
    }

    public function getZSNag(): ?string
    {
        return $this->ZS_Nag;
    }

    public function setZSNag(string $ZS_Nag): self
    {
        $this->ZS_Nag = $ZS_Nag;

        return $this;
    }

    public function getKntAkronim(): ?string
    {
        return $this->Knt_Akronim;
    }

    public function setKntAkronim(string $Knt_Akronim): self
    {
        $this->Knt_Akronim = $Knt_Akronim;

        return $this;
    }

    public function getPZLId(): ?int
    {
        return $this->PZL_Id;
    }

    public function setPZLId(int $PZL_Id): self
    {
        $this->PZL_Id = $PZL_Id;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return Collection|ZaplanowanyProces[]
     */
    public function getZaplanowane(): Collection
    {
        return $this->zaplanowane;
    }

    public function addZaplanowane(ZaplanowanyProces $zaplanowane): self
    {
        if (!$this->zaplanowane->contains($zaplanowane)) {
            $this->zaplanowane[] = $zaplanowane;
            $zaplanowane->setZlecenie($this);
        }

        return $this;
    }

    public function removeZaplanowane(ZaplanowanyProces $zaplanowane): self
    {
        if ($this->zaplanowane->contains($zaplanowane)) {
            $this->zaplanowane->removeElement($zaplanowane);
            // set the owning side to null (unless already changed)
            if ($zaplanowane->getZlecenie() === $this) {
                $zaplanowane->setZlecenie(null);
            }
        }

        return $this;
    }

    public function getKolor(): ?string
    {
        return $this->kolor;
    }

    public function setKolor(string $kolor): self
    {
        $this->kolor = $kolor;

        return $this;
    }
}
