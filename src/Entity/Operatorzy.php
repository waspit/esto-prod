<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OperatorzyRepository")
 */
class Operatorzy
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $imie;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nazwisko;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $numerKarty;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $typKarty;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getImie(): ?string
    {
        return $this->imie;
    }

    public function setImie(string $imie): self
    {
        $this->imie = $imie;

        return $this;
    }

    public function getNazwisko(): ?string
    {
        return $this->nazwisko;
    }

    public function setNazwisko(string $nazwisko): self
    {
        $this->nazwisko = $nazwisko;

        return $this;
    }

    public function getNumerKarty(): ?string
    {
        return $this->numerKarty;
    }

    public function setNumerKarty(string $numerKarty): self
    {
        $this->numerKarty = $numerKarty;

        return $this;
    }

    public function getTypKarty(): ?string
    {
        return $this->typKarty;
    }

    public function setTypKarty(string $typKarty): self
    {
        $this->typKarty = $typKarty;

        return $this;
    }
}
