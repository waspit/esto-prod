<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TestRepository")
 */
class Test
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     */
    private $testt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTestt(): ?float
    {
        return $this->testt;
    }

    public function setTestt(float $testt): self
    {
        $this->testt = $testt;

        return $this;
    }
}
