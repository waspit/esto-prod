<?php

namespace App\Entity;

use App\Repository\OpcjeRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OpcjeRepository::class)
 */
class Opcje
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $listaGotowe;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $listaWidoczne;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getListaGotowe(): ?bool
    {
        return $this->listaGotowe;
    }

    public function setListaGotowe(?bool $listaGotowe): self
    {
        $this->listaGotowe = $listaGotowe;

        return $this;
    }

    public function getListaWidoczne(): ?bool
    {
        return $this->listaWidoczne;
    }

    public function setListaWidoczne(?bool $listaWidoczne): self
    {
        $this->listaWidoczne = $listaWidoczne;

        return $this;
    }
}
