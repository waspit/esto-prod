<?php

declare(strict_types=1);

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

final class ProcesProdukcjiDoZleceniaAdmin extends AbstractAdmin
{

    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            ->add('id')
            ->add('nazwa')
            ->add('przezbrojenieTechnologi')
            ->add('czasPrzezbrojenia')
            ->add('czasCykluOCC')
            ->add('krotnosc')
            ->add('dataProdukcji')
            ;
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
            ->add('id')
            ->add('nazwa')
            ->add('przezbrojenieTechnologi')
            ->add('czasPrzezbrojenia')
            ->add('czasCykluOCC')
            ->add('krotnosc')
            ->add('dataProdukcji')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ]);
    }

    protected function configureFormFields(FormMapper $formMapper): void
    {
        $formMapper
            
            ->add('nazwa', null, ['label' => 'Nazwa procesu produkcji'])
            ->add('przezbrojenieTechnologi', null, ['label' => 'Wymagane przezbrojenie technologii'])
            ->add('czasPrzezbrojenia', null, ['label' => 'Czas przezbrojenia'])
            ->add('czasCykluOCC', null, ['label' => 'Czas cyklu OCC'])
            ->add('krotnosc', null, ['label' => 'Krotność'])
            ->add('dataProdukcji', null, ['label' => 'Data rozpoczęcia produkcji'])
            ;
    }

    protected function configureShowFields(ShowMapper $showMapper): void
    {
        $showMapper
            ->add('id')
            ->add('nazwa')
            ->add('przezbrojenieTechnologi')
            ->add('czasPrzezbrojenia')
            ->add('czasCykluOCC')
            ->add('krotnosc')
            ->add('dataProdukcji')
            ;
    }
}
