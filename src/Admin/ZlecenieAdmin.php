<?php

declare(strict_types=1);

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

final class ZlecenieAdmin extends AbstractAdmin
{

    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            ->add('id')
            ->add('nrZP')
            ->add('nrZS')
            ->add('kodProduktu')
            ->add('nazwaProduktu')
            ->add('akronim')
            ->add('zamowionaIlosc')
            ;
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
            ->add('id')
            ->add('nrZP')
            ->add('nrZS')
            ->add('kodProduktu')
            ->add('nazwaProduktu')
            ->add('akronim')
            ->add('zamowionaIlosc')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ]);
    }

    protected function configureFormFields(FormMapper $formMapper): void
    {
        $formMapper
            ->add('nrZP', null, ['label' => 'Numer zlecenia ZP'])
            ->add('nrZS', null, ['label' => 'Numer zlecenia ZS'])
            ->add('kodProduktu', null, ['label' => 'Kod produktu'])
            ->add('nazwaProduktu', null, ['label' => 'Nazwa produktu'])
            ->add('akronim', null, ['label' => 'Akronim'])
            ->add('zamowionaIlosc', null, ['label' => 'Zamówiona ilość'])
            ;
    }

    protected function configureShowFields(ShowMapper $showMapper): void
    {
        $showMapper
            ->add('id')
            ->add('nrZP')
            ->add('nrZS')
            ->add('kodProduktu')
            ->add('nazwaProduktu')
            ->add('akronim')
            ->add('zamowionaIlosc')
            ;
    }
}
