<?php

declare(strict_types=1);

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

final class OperatorzyAdmin extends AbstractAdmin
{

    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            ->add('id')
            ->add('imie')
            ->add('nazwisko')
            ->add('numerKarty')
            ->add('typKarty')
            ;
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
            ->add('id')
            ->add('imie')
            ->add('nazwisko')
            ->add('numerKarty')
            ->add('typKarty')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ]);
    }

    protected function configureFormFields(FormMapper $formMapper): void
    {
        $formMapper
            
            ->add('imie', null, ['label' => 'Imię'])
            ->add('nazwisko', null, ['label' => 'Nazwisko'])
            ->add('numerKarty', null, ['label' => 'Numer karty'])
            ->add('typKarty', null, ['label' => 'Typ karty'])
            ;
    }

    protected function configureShowFields(ShowMapper $showMapper): void
    {
        $showMapper
            ->add('id')
            ->add('imie')
            ->add('nazwisko')
            ->add('numerKarty')
            ->add('typKarty')
            ;
    }
}
