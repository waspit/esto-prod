$(document).ready(function() {
  $(".czascyk").keypress(function(event) {
    if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
      event.preventDefault();
    }
  });
  $(".czasprze").keypress(function(event) {
    if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
      event.preventDefault();
    }
  });
  $(".krotnosc").keypress(function(event) {
    if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
      event.preventDefault();
    }
  });
$( "#dodaj-proces" ).click(function() {
    var $div = $('div[id^="proces"]:last');

    // Read the Number from that DIV's ID (i.e: 3 from "klon3")
    // And increment that number by 1
    var num = parseInt( $div.prop("id").match(/\d+/g), 10 ) +1;
  
    // Clone it and assign the new ID (i.e: from num 4 to ID "klon4")
    var $klon = $div.clone().prop('id', 'proces'+num );
  
    // Finally insert $klon wherever you want
    $klon.find(".nazwa").text("Proces produkcji "+num);
    $klon.find(".proces").prop('name', "proces-"+num);
    $klon.find(".data").prop('name', "data-"+num);
    $klon.find(".przezbrojenie").prop('name', "przezbrojenie-"+num);
    $klon.find(".hprzezbrojenie").prop('name', "przezbrojenie-"+num);
    $klon.find(".czasprze").prop('name', "czasprze-"+num);
    $klon.find(".czascyk").prop('name', "czascyk-"+num);
    $klon.find(".czascyk").val("");
    $klon.find(".krotnosc").prop('name', "krotnosc-"+num);
    $klon.find(".krotnosc").val("");
    $klon.find(".dtpicker").prop('id', "dtp-"+num);
    $klon.find(".dtpicker").prop('name', "data-"+num);
    $div.after( $klon);
    jQuery("#dtp-"+num).datetimepicker({
        step: 15
      });
});
});